let dark = false;
var hab1 = document.getElementById("hab1");
var hab2 = document.getElementById("hab2");
function anoitecer() {
    dark = !dark;
    if(dark === true) {
        document.body.style.background = "#303030";
        document.body.style.color = "#ffffff"; 
        hab1.style.background = "#303030";
        hab1.style.color = "#ffffff";
        hab2.style.background = "#303030";
        hab2.style.color = "#ffffff";
    }
    else {
        document.body.style.background = "#ffffff";
        document.body.style.color = "#303030";
        hab1.style.background = "#ffffff";
        hab1.style.color = "#303030";
        hab2.style.background = "#ffffff";
        hab2.style.color = "#303030";
    }
}